import React from "react";
import "./List.css";
import { getHighlightedText } from "../../helper/helper";
const List = ({ data, highlighted, isReplace, isReplaceAll, replacedText }) => {
  return (
    <tr>
      <td className="list--element"> {data.title}</td>
      <td>
        <div className="list--element">
          {getHighlightedText(
            data.snippet,
            highlighted === "" ? "Liverpool" : highlighted,
            isReplace,
            isReplaceAll,
            replacedText
          )}
        </div>
      </td>
    </tr>
  );
};
export default List;
