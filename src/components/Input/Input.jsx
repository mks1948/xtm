import React from "react";
import "./Input.css"

const Input = ({ type ,handleChangeSearch , value}) => {
  return (
    <input
	    id={type}
	    className="nav__container--element"
      type="text"
      placeholder={type}
      value={value}
      onChange={e => handleChangeSearch(e.target.value , type)}
    />
  );
};
export default Input;
