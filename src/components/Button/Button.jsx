import React from "react";
import "./Button.css";
const Button = ({ type, handleClickButton, isActive }) => {
  return (
    <button
      className={
        isActive ? "nav__container--active" : "nav__container--element"
      }
      onClick={e => handleClickButton(e, type)}
    >
      {type}
    </button>
  );
};
export default Button;
