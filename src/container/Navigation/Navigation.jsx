import React, { useEffect, useState } from "react";
import Input from "../../components/Input/Input";
import Button from "../../components/Button/Button";
import axios from "axios";
import "./Navigation.css";
import List from "../../components/List/List";
import { getAllIndexes } from "../../helper/helper";

const configEnum = {
  search: "search",
  replace: "replace",
  searchButton: "Search",
  replaceButton: "Replace",
  replaceAllButton: "Replace All"
};

const Navigation = () => {
  const [initData, setData] = useState([]);
  const [tempData, setTempData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [searchText, setSearchText] = useState("");
  const [replaceText, setReplaceText] = useState("");
  const [isReplace, setIsReplace] = useState(false);
  const [isReplaceAll, setIsReplaceAll] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    fetchData("init");
  }, []);

  const fetchData = type => {
    const query = () => {
      if (type === "init") {
        return "Liverpool";
      }
      if (type === configEnum.searchButton && searchText.length > 0) {
        return searchText;
      } else {
        return "Liverpool";
      }
    };

    axios
      .get(
        `${
          process.env.REACT_APP_API
        }action=query&list=search&format=json&srsearch=${query()}&srlimi&origin=*`
      )
      .then(({ status, data: { query: { search } } }) => {
        if (status === 200) {
          setData(search);
          setTempData(search);
          setIsLoading(false);
        }
      })
      .catch(e => {
        setIsLoading(false);
      });
  };

  const handleChangeSearch = (value, type) => {
    if (type === configEnum.search) {
      if (value === "") {
        setIsReplace(false);
        setIsReplaceAll(false);
      }
      setSearchText(value);
      setReplaceText("");
      setIsReplaceAll(false);
    }
    if (type === configEnum.replace) {
      setReplaceText(value);
    }
  };

  const handleChangeReplaceFirstResult = isClear => {
    let found = isClear ? false : true;
    const tempVar = JSON.parse(JSON.stringify(initData));

    tempVar.forEach(item => {
      if (found === false) {
        const parts = item.snippet
          .toLowerCase()
          .split(new RegExp(`(${searchText.toLowerCase()})`, "gi"));

        const indexes = getAllIndexes(parts, searchText.toLowerCase());
        if (indexes.length > 0) {
          const firstValue = indexes[0];
          parts[firstValue] = replaceText;
        } else {
          parts[indexes] = replaceText;
        }
        const text = parts.join(" ");
        item.snippet = text;
        found = true;
      }
    });
    
    setIsReplace(true);
    setTempData(tempVar);
  };

  const handleClickButton = (e, type) => {
    const checkIsFill = () => {
      return replaceText.length > 0 && searchText.length > 0;
    };

    if (type === configEnum.searchButton) {
      fetchData(configEnum.searchButton);
      setReplaceText("");
      setIsReplace(false);
      setIsReplaceAll(false);
    }
    if (type === configEnum.replaceButton && checkIsFill()) {
      handleChangeReplaceFirstResult(true);
      setIsReplaceAll(false);
    }
    if (type === configEnum.replaceAllButton && checkIsFill()) {
      setIsReplace(false);
      setIsReplaceAll(true);
    }
  };

  const handleReturnArray = () => {
    if (isReplace) {
      return tempData;
    } else {
      return initData;
    }
  };

  return (
    <>
      <nav className="nav__container">
        <Input
          type={configEnum.search}
          handleChangeSearch={handleChangeSearch}
          value={searchText}
        />
        <Input
          type={configEnum.replace}
          handleChangeSearch={handleChangeSearch}
          value={replaceText}
        />
        <Button
          type={configEnum.searchButton}
          handleClickButton={handleClickButton}
        />
        <Button
          type={configEnum.replaceButton}
          isActive={isReplace}
          handleClickButton={handleClickButton}
        />
        <Button
          type={configEnum.replaceAllButton}
          isActive={isReplaceAll}
          handleClickButton={handleClickButton}
        />
      </nav>
      {!isLoading ? (
        <table>
          <tbody>
            <tr>
              <th>Title</th>
              <th>Snippet</th>
            </tr>
            {initData.length > 0 &&
              handleReturnArray().map((item, key) => {
                return (
                  <List
                    key={key}
                    data={item}
                    highlighted={isReplace ? replaceText : searchText}
                    isReplace={isReplace}
                    replacedText={replaceText}
                    isReplaceAll={isReplaceAll}
                  />
                );
              })}
          </tbody>
        </table>
      ) : (
        "Loading"
      )}
    </>
  );
};
export default Navigation;
