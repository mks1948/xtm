import React from "react";

export const getAllIndexes = (arr, val) => {
  let indexes = [],
    i = -1;
  while ((i = arr.indexOf(val, i + 1)) !== -1) {
    indexes.push(i);
  }
  return indexes;
};

export const getHighlightedText = (
  text,
  highlighted,
  isReplace,
  isReplaceAll,
  replacedText
) => {
  const parts = text
    .toLowerCase()
    .split(new RegExp(`(${highlighted.toLowerCase()})`, "gi"));
  if (isReplaceAll && !isReplace) {
    const indexes = getAllIndexes(parts, highlighted.toLowerCase());
    if (indexes.length > 1) {
      parts.forEach((item, key) => {
        if (item.toLowerCase() === highlighted.toLowerCase()) {
          parts[key] = replacedText;
        }
      });
    } else {
      parts[indexes] = replacedText;
    }
  }
  return (
    <span>
      {parts.map((part, i) => {
        return (
          <span
            key={i}
            style={
              part.toLowerCase() === highlighted.toLowerCase() ||
              part.toLowerCase() === replacedText.toLowerCase()
                ? { color: "red", fontWeight: "bold" }
                : { color: "black" }
            }
            dangerouslySetInnerHTML={{
              __html: part
            }}
          />
        );
      })}
    </span>
  );
};
