import React from "react";
import "./App.css";
import Navigation from "./container/Navigation/Navigation";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>Recruitment task for XTM</p>
      </header>
      <Navigation />
    </div>
  );
}

export default App;
